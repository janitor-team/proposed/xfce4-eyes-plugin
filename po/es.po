# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Abel Martín <abel.martin.ruiz@gmail.com>, 2008
# Jaime Buffery <nestu@lunar-linux.org>, 2004
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 06:32+0200\n"
"PO-Revision-Date: 2019-08-02 04:32+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Spanish (http://www.transifex.com/xfce/xfce-panel-plugins/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/eyes.c:367 ../panel-plugin/eyes.desktop.in.h:1
msgid "Eyes"
msgstr "Ojos"

#: ../panel-plugin/eyes.c:370
msgid "_Close"
msgstr "_Cerrar"

#: ../panel-plugin/eyes.c:387
msgid "_Select a theme:"
msgstr "_Seleccionar un tema:"

#: ../panel-plugin/eyes.c:423
msgid "Use single _row in multi-row panel"
msgstr "Utilice una _fila única en el panel de varias filas"

#: ../panel-plugin/eyes.desktop.in.h:2
msgid "Eyes that spy on you"
msgstr "Ojos que te espían"
